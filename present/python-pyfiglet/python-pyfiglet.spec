%global pypi_name   pyfiglet
%global forgeurl    https://github.com/pwaller/pyfiglet

Name:           python-%{pypi_name}
Version:        0.8.post1
Release:        %autorelease
Summary:        Pure-python FIGlet implementation

%global commit      31c21be637c722cd7100b19df8f6efab31accf9c
%global shortcommit %(c=%{commit}; echo ${c:0:7})
#forgemeta

# Only figlet's fonts seem to be under BSD and MIT, those are:
# - banner.flf
# - big.flf
# - block.flf
# - bubble.flf
# - digital.flf
# - ivrit.flf
# - lean.flf
# - mini.flf
# - mnemonic.flf
# - script.flf
# - shadow.flf
# - slant.flf
# - small.flf
# - smscript.flf
# - smshadow.flf
# - smslant.flf
# - standard.flf
# - term.flf
#
# Those have already been approved in Fedora as part of the figlet package.
# Though, it is not really clear if those fonts are under BSD or MIT. The
# figlet package lists both, and both do not affect this package's licensing.
# All other fonts are under a functional equivalent of MIT. Each font has its
# License in its header.
License:        MIT and BSD
URL:            %{forgeurl}
Source0:        pyfiglet-%{shortcommit}-no-contrib-font.tar.gz
# Cleans all fonts in pyfiglet/fonts-contrib
# USAGE:
#   ./generate-pyfiglet-tarball.sh COMMIT
Source1:        generate-pyfiglet-tarball.sh
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-pytest


%global _description %{expand:
pyfiglet is a full port of FIGlet (http://www.figlet.org/) into pure python. It
takes ASCII text and renders it in ASCII art fonts (like the title above, which
is the 'block' font).}

%description %{_description}


%package -n     python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name} %{_description}


%prep
%autosetup -n %{pypi_name}-%{shortcommit}

mkdir pyfiglet/fonts
cp pyfiglet/fonts-standard/* pyfiglet/fonts

%generate_buildrequires
%pyproject_buildrequires -r


%build
%pyproject_wheel


%install
%pyproject_install

%pyproject_save_files %{pypi_name}

# Resolve RPMLint errors
sed -i '1d' %{buildroot}%{python3_sitelib}/%{pypi_name}/{__init__,test}.py

install -Dpm 0644 -t %{buildroot}%{_mandir}/man1/ doc/%{pypi_name}.1


%check
%py3_check_import pyfiglet
# Tests don't work: Missing subprocess32 module.
#pytest


%files -n python3-%{pypi_name} -f %{pyproject_files}
%doc README doc/figfont.txt
%license LICENSE
%{_bindir}/%{pypi_name}
%{_mandir}/man1/%{pypi_name}.1.*


%changelog
%autochangelog
