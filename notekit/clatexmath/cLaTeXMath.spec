%global commit      dc32540664276a00805b21a0fb5cd6418bacdb54
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           cLaTeXMath
Version:        0.0.4
Release:        1%{?dist}
Summary:        A dynamic and cross-platform LaTeX rendering library

%forgemeta

# MIT is the License of cLaTeXMath.
# The fonts are under different licenses:
# - eufb10.ttf, eufm10.ttf, msam10.ttf and msbm10.ttf are under OFL
# - dsrom10.ttf is under the DoubleStroke License
# - stmary10.ttf and rsfs10.ttf are in the Public Domain
# - cmbsy10.ttf, cmbx10.ttf, cmbxti10.ttf, cmex10.ttf, cmmi10.ttf, cmr10.ttf,
# cmss10.ttf, cmssbx10.ttf, cmsy10.ttf and cmtt10.ttf are under the Knuth
# License
# - the greek fonts under res/greek/ are under the GPLv2+ License
# - the cyrillic fonts under res/cyrillic/ are under the Knuth License
# - the .xml files in the res/greek/ and res/cyrillic/ folders are under GPLv2+
License:        MIT and zlib and OFL and Knuth and DoubleStroke and GPLv2+ and Public Domain
URL:            https://github.com/NanoMichael/cLaTeXMath

Source0:        %{name}-%{shortcommit}-clean.tar.gz
# cLaTeXMath bundle a font named special.ttf which is a mashup of other fonts
# and thus has no clear license at all. See :
# https://github.com/opencollab/jlatexmath/issues/80
# This script downloads the tarball & remove the font and all its references in
# the code:
# ./generate-cLaTeXMath-tarball.sh COMMIT
Source1:        generate-cLaTeXMath-tarball.sh
Source2:        special.ttf

BuildRequires:  meson
BuildRequires:  gcc-c++
BuildRequires:  git-core
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(gtkmm-3.0)
BuildRequires:  pkgconfig(gtksourceviewmm-3.0)
BuildRequires:  pkgconfig(tinyxml2)

%global _description %{expand:
It is a dynamic, cross-platform, and embeddable LaTeX rendering library. Its
main purpose is to display mathematical formulas written in LaTeX. It can be
embedded in applications on various platforms (Android, iOS, Windows, Linux GTK,
Qt...). The following pictures demonstrate the application run in Ubuntu (using
GTK) and Windows.}

%description %_description


%package devel
Summary:        %{summary}

Requires:       %{name}%{?_isa} = %{version}-%{release}

Requires:       pkgconfig(fontconfig)
Requires:       pkgconfig(gtkmm-3.0)
Requires:       pkgconfig(gtksourceviewmm-3.0)

%description devel %_description


%package doc
Summary:        Additional documentation for cLaTeXMath

BuildArch:      noarch

%description doc %_description


%prep
%autosetup -n cLaTeXMath-%{commit} -S git

sed -e '0,/^# License/d' README.md > LICENSE

install -p %{SOURCE2} res/fonts/maths/


%build
%meson
%meson_build

cp res/cyrillic/LICENSE GPLv2+.txt


%install
%meson_install


%files
%doc README.md
%license LICENSE res/fonts/licences/* GPLv2+.txt
%{_datadir}/clatexmath
%{_libdir}/libclatexmath.so.0
%{_libdir}/libclatexmath.so.0.0.4


%files devel
%{_includedir}/clatexmath
%{_libdir}/libclatexmath.so
%{_libdir}/pkgconfig/clatexmath.pc


%files doc
%doc readme


%changelog
* Sun Jul 11 2021 Lyes Saadi <fedora@lyes.eu> - 0.0.4-1
- Rebasing to newer commit: dc32540664276a00805b21a0fb5cd6418bacdb54
- Including custom special.ttf

* Thu May 13 2021 Lyes Saadi <fedora@lyes.eu> - 0.0.3-4
- Unbundling tinyxml2
- Fixing licensing issues
- Removing special.ttf
- Rebasing to newer commit: 71b3352b4ec67be13a446515ee4662d6c3f22bc4

* Mon Mar 15 2021 Lyes Saadi <fedora@lyes.eu> - 0.0.3-3
- Adding a detailed licensing breakdown
- Adding the GPLv2+ License

* Sun Mar 14 2021 Lyes Saadi <fedora@lyes.eu> - 0.0.3-2
- Adding a -doc subpackage
- Separating the LICENSE from the README.md

* Sun Jan 17 2021 Lyes Saadi <fedora@lyes.eu> - 0.0.3-1
- Updating to latest version
- Following the weird upstream versioning

* Tue Jan 12 2021 Lyes Saadi <fedora@lyes.eu> - 0-2
- Updating to latest version

* Wed Nov 18 2020 Lyes Saadi <fedora@lyes.eu> - 0-1
- Initial package
