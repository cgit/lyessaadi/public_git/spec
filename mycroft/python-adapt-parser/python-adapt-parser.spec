%global pypi_name adapt-parser

Name:           python-%{pypi_name}
Version:        1.0.0
Release:        1%{?dist}
Summary:        A text-to-intent parsing framework

License:        ASL 2.0
URL:            https://github.com/MycroftAI/adapt
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
The Adapt Intent Parser is a flexible and extensible intent definition and
determination framework. It is intended to parse natural language text into a
structured intent that can then be invoked programatically.
}

%description %_description

%{?python_disable_dependency_generator}

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(ee) >= 8.1
Requires:       python3dist(six) >= 1.10

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE.md
%{python3_sitelib}/adapt
%{python3_sitelib}/adapt_parser-%{version}-py%{python3_version}.egg-info

%changelog
* Thu Sep 30 2021 Lyes Saadi <fedora@lyes.eu> - 1.0.0-1
- Updating to 1.0.0

* Mon Sep 13 2021 Lyes Saadi <fedora@lyes.eu> - 0.6.1-1
- Updating to 0.6.1

* Sun Sep 12 2021 Lyes Saadi <fedora@lyes.eu> - 0.6.0-1
- Updating to 0.6.0

* Sun Aug 15 2021 Lyes Saadi <fedora@lyes.eu> - 0.5.1-1
- Updating to 0.5.1

* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.3.5-1
- Initial Package
