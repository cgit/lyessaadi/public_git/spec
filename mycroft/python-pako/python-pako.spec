%global pypi_name pako

Name:           python-%{pypi_name}
Version:        0.2.3
Release:        1%{?dist}
Summary:        The universal package manager library

License:        ASL 2.0
URL:            https://github.com/MycroftAI/pako
Source0:        %{pypi_source}
BuildArch:      noarch

# Adding DNF/rpm-ostree compatibility.
Patch0:		https://patch-diff.githubusercontent.com/raw/MycroftAI/pako/pull/4.patch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%global _description %{expand:
Often, scripts need to install system dependencies using the native package
manager of the user's OS. Typically, this is solved by having some bash script
that runs apt-get, assuming the user is on Ubuntu. Smarter scripts use hand
crafted code to detect the user's platform and aggregate a set of dependencies
on a few of the more popular platforms. Our approach is different:

pako install libgdbm-devel sdl2-dev

On Ubuntu, this command will run:

sudo apt-get install -y libgdbm-dev libsdl2-dev

However, on Solus, this will run:

sudo eopkg install -y gdbm-devel sdl2-devel

It works as follows:

- Parse package format (devel/debug/normal library or executable)
- Look up package managers that exist in PATH
- Format parsed package with common package convention of package manager
}

%description %_description

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(appdirs)
Requires:       python3dist(setuptools)

%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version} -p1
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install
rm %{buildroot}%{_prefix}/%{pypi_name}/LICENSE

%files -n python3-%{pypi_name}
%license LICENSE
%{_bindir}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu> - 0.2.3-1
- Initial Package
