%global pypi_name lingua-franca

%global tag 0.4.2

Name:           python-%{pypi_name}
Version:        %{tag}
Release:        1%{?dist}
Summary:        Mycroft's multilingual text parsing and formatting library

%global forgeurl https://github.com/MycroftAI/lingua-franca
%forgemeta

License:        ASL 2.0
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildArch:      noarch

%global _description %{expand:
Lingua Franca (noun): a framework that is adopted as the common language between
speakers with different native tongues.
}

%description %_description

%{?python_disable_dependency_generator}

%package -n     python3-%{pypi_name}
Summary:        %{summary}

Requires:       python3-dateutil
BuildRequires:  python3-devel
BuildRequires:  python3-dateutil

%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name} %_description

%prep
%forgeautosetup

%build
%py3_build

%install
%py3_install
rm -rf %{buildroot}%{python3_sitelib}/test

#%%check
#%%{python3} setup.py test

%files -n       python3-%{pypi_name}
%doc readme.md
%license LICENSE
%{python3_sitelib}/lingua_franca/
%{python3_sitelib}/lingua_franca-*.egg-info/

%changelog
* Sun Aug 15 2021 Lyes Saadi <fedora@lyes.eu> - 0.4.2-1
- Updating to 0.4.2

* Sat Jun 06 2020 Lyes Saadi <fedora@lyes.eu>
- Initial Package
