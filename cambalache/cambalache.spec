%global forgeurl    https://gitlab.gnome.org/jpu/cambalache

%global uuid        ar.xjuan.Cambalache

Name:           cambalache
Version:        0.8.1
Release:        %autorelease
Summary:        RAD tool for Gtk 4 & 3 with a clear MVC design and data model first philosophy
BuildArch:      noarch

%forgemeta

# Cambalache is licensed under the LGPLv2 license.
# Tools (in the tools/ directory) are licensed under the GPLv2 license.
# Tools are not installed, so neither will be the GPLv2 license.
License:        LGPLv2 AND GPLv2
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  meson
BuildRequires:  gtk4-devel
BuildRequires:  gtk3-devel
BuildRequires:  python3-devel
BuildRequires:  python3-gobject-devel
BuildRequires:  python3-lxml
BuildRequires:  pytest
BuildRequires:  webkit2gtk3
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib

Requires:       gtk4
Requires:       hicolor-icon-theme
Requires:       python3-lxml
Requires:       webkit2gtk3
Recommends:     gtk3


%global _description %{expand:
Cambalache is a new RAD tool for Gtk 4 and 3 with a clear MVC design and data
model first philosophy. This translates to a wide feature coverage with
minimal/none developer intervention for basic support.

To support multiple Gtk versions it renders the workspace out of process using
the Gdk broadway backend.}

%description %{_description}


%prep
%forgeautosetup


%build
%meson
%meson_build


%install
%meson_install
%find_lang %{name}


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/%{uuid}.desktop


%files -f %{name}.lang
%license COPYING
%doc README.md
%{_bindir}/%{name}
%{python3_sitelib}/%{name}/
%{_metainfodir}/%{uuid}.metainfo.xml
%{_datadir}/applications/%{uuid}.desktop
%{_datadir}/%{name}/
%{_datadir}/glib-2.0/schemas/%{uuid}.gschema.xml
%{_datadir}/icons/hicolor/*/{apps,mimetypes}/*.svg
%{_datadir}/mime/packages/%{uuid}.mime.xml


%changelog
%autochangelog
