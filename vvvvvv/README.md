# VVVVVV

So, this was done after VVVVVV's source code was released. Unfortunately,
because of a too strict licensing, I decided a while ago not to submit it to
Fedora or RPM Fusion's repositories.

But, in order for my work not to go to waste, here are the spec file to build
yourself the RPM if you wish to. Just keep in mind that redistributing that RPM
would be illegal.

## Instructions

```
sudo dnf install @fedora-packager
sudo dnf builddep VVVVVV.spec
rpmdev-setuptree
cp VVVVVV.appdata.xml VVVVVV.desktop ~/rpmbuild/SOURCES/
spectool -gR VVVVVV.spec
rpmbuild -ba VVVVVV.spec
```

If you wish to update VVVVVV:

1. Open VVVVVV.spec with your favorite editor.
2. Change the commit in the line beginning with `%global commit`.
3. In the line beginning with `Release:`, increase the first number (ex: 1->2).
4. `spectool -gR VVVVVV.spec`
5. `rpmbuild -ba VVVVVV.spec`
