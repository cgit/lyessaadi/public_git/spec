%global         uuid        org.gabmus.giara
%global         forgeurl    https://gitlab.gnome.org/World/giara

Name:           giara
Version:        1.0
Release:        1%{?dist}
Summary:        GTK client for Reddit, built with mobile Linux in mind

%global         tag         %{version} 

%forgemeta

License:        GPLv3+
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  meson
BuildRequires:  python3-devel
BuildRequires:  gtk4-devel
BuildRequires:  libadwaita-devel
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  python3-beautifulsoup4
BuildRequires:  python3-gobject
BuildRequires:  python3-mistune
BuildRequires:  python3-pillow
BuildRequires:  python3-praw
BuildRequires:  python3-requests

Requires:       gtk4
Requires:       hicolor-icon-theme
Requires:       libadwaita
Requires:       python3-beautifulsoup4
Requires:       python3-gobject
Requires:       python3-mistune
Requires:       python3-pillow
Requires:       python3-praw
Requires:       python3-requests

BuildArch:      noarch

# Giara was once named redditgtk
Provides:       redditgtk = %{version}-%{release}
Obsoletes:      redditgtk <= 0.2

%description
Giara is a reddit app, built with Python, GTK4 and libadwaita. Created with
mobile Linux in mind.


%prep
%forgeautosetup

sed -i "78d" meson.build


%build
%meson
%meson_build


%install
%meson_install

%find_lang %{name}


%files -f %{name}.lang
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/%{uuid}.desktop
%{_datadir}/dbus-1/services/%{uuid}.service
%{_datadir}/glib-2.0/schemas/%{uuid}.gschema.xml
%{_datadir}/icons/hicolor/*/apps/%{uuid}*.svg
%{_metainfodir}/%{uuid}.appdata.xml
%{python3_sitelib}/%{name}/


%changelog
* Mon Aug 30 2021 Lyes Saadi <fedora@lyes.eu> - 1.0-1
- Updating to 1.0

* Wed Nov 18 2020 Lyes Saadi <fedora@lyes.eu> - 0.3-1
- Updating to 0.3

* Wed Oct 21 2020 Lyes Saadi <fedora@lyes.eu> - 0.2-1
- Updating to 0.2

* Sun Oct 11 2020 Lyes Saadi <fedora@lyes.eu> - 0.1.1-1
- Updating to 0.1.1

* Sat Oct 10 2020 Lyes Saadi <fedora@lyes.eu> - 0.1-1
- 0.1 Release
- Rename package from redditgtk to giara

* Sun Sep 20 2020 Lyes Saadi <fedora@lyes.eu> - 0-1.20200920gitc7406bc
- Initial package
