%global shortname polling

Name:           python-%{shortname}
Version:        0.3.0
Release:        1%{?dist}
Summary:        Powerful polling utility in Python

License:        MIT
URL:            https://github.com/justiniso/%{shortname}
Source0:        %{url}/archive/v%{version}/%{shortname}-%{version}.tar.gz

BuildArch:      noarch

%global _description %{expand:
Polling is a powerful python utility used to wait for a function to return a certain expected condition. Some possible uses cases include:

- Wait for API response to return with code 200
- Wait for a file to exist (or not exist)
- Wait for a thread lock on a resource to expire
}

%description    %{_description}

%package        -n python3-%{shortname}
Summary:        %{Summary}
BuildRequires:  python3-devel
BuildRequires:  python3-mock
BuildRequires:  python3-pytest
%{?python_provide:%python_provide python3-%{shortname}}

%description    -n python3-%{shortname} %{_description}

%prep
%autosetup      -n %{shortname}-%{version}

%build
%py3_build

%install
%py3_install

%check
py.test tests

%files          -n python3-%{shortname}
%license LICENSE.md
%doc README.md
%pycached %{python3_sitelib}/%{shortname}.py
%{python3_sitelib}/%{shortname}-%{version}-py*.egg-info

%changelog
* Sun Mar 29 2020 Lyes Saadi <fedora@lyes.eu> - 0.3.0-1
- Initial package.
