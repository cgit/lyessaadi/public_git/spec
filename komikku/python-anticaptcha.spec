%global shortname anticaptcha

Name:           python-%{shortname}
Version:        0.5.0
Release:        1%{?dist}
Summary:        Client library for solve captchas with Anticaptcha.com support

License:        MIT
URL:            https://github.com/ad-m/python-%{shortname}
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:      noarch

%global _description %{expand:
WARNING: This is python_anticaptcha and NOT python3_anticaptcha.

Client library for solve captchas with Anticaptcha.com support. The library
supports both Python 2.7 and Python 3.

The library is cyclically and automatically tested for proper operation. We are
constantly making the best efforts for its effective operation.}

%description    %{_description}

%package        -n python3-%{shortname}
Summary:        %{Summary}
BuildRequires:  git
BuildRequires:  python3-devel
BuildRequires:  python3-nose
BuildRequires:  python3-requests
BuildRequires:  python3-retry
BuildRequires:  python3-selenium
BuildRequires:  python3-six
Requires:       python3-requests
Requires:       python3-six
%{?python_provide:%python_provide python3-%{shortname}}

%description    -n python3-%{shortname} %{_description}

%package        doc
Summary:        Documentation for python_anticaptcha
BuildRequires:  python3-sphinx

%description    doc
Documentation files for python_anticaptcha built using sphinx.

%prep
%autosetup
git init
git add .
git commit -am "Version %{version}"
git tag -am "Version %{version}" %{version}

%build
%py3_build
sphinx-build docs html
rm -rf html/.{doctrees,buildinfo}

%install
%py3_install
mkdir -p %{buildroot}%{python3_sitelib}/python_anticaptcha
cp -pr python_anticaptcha %{buildroot}%{python3_sitelib}/

%check
# Some test won't run unless an API key is provided...
# Feel free to add one for your personnal use, not sure if Fedora is allowed to
# use one though...
nosetests tests --verbosity=3 --processes=8 --process-timeout=1200

%files          -n python3-%{shortname}
%license LICENSE.md
%doc README.rst CHANGELOG.rst examples
%{python3_sitelib}/python_%{shortname}
%{python3_sitelib}/python_%{shortname}-%{version}-py*.egg-info

%files          doc
%doc html examples

%changelog
* Tue Mar 31 2020 Lyes Saadi <fedora@lyes.eu> - 0.5.0-1
- Initial package.
