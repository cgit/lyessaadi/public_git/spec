Name:           tootle
Version:        0.2.0
Release:        1%{?dist}
Summary:        GTK3 client for Mastodon

License:        GPLv3
URL:            https://github.com/bleakgrey/tootle
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  granite-devel
BuildRequires:  gtk3-devel
BuildRequires:  json-glib-devel
BuildRequires:  libsoup-devel
BuildRequires:  meson
BuildRequires:  vala
#Requires:       test

%description
Simple Mastodon client designed for elementary OS and made in GTK3.


%prep
%autosetup


%build
%meson
%meson_build


%install
%meson_install
%find_lang %{name}


%check
%meson_test


%files -f %{name}.lang
%license LICENSE
%doc README.md


%changelog
* Fri Apr  3 2020 Lyes Saadi <fedora@lyes.eu>
- Initial Package.
