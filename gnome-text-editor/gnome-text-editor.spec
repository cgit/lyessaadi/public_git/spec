%global forgeurl    https://gitlab.gnome.org/GNOME/gnome-text-editor
%global uuid        org.gnome.TextEditor

Name:           gnome-text-editor
Version:        41.1
Release:        %autorelease
Summary:        A simple text editor

%global tag     %{version}
%forgemeta

License:        GPLv3+
URL:            %{forgeurl}
Source0:        %{forgesource}

BuildRequires:  meson
BuildRequires:  itstool
BuildRequires:  gtk4-devel
BuildRequires:  gtksourceview5-devel
BuildRequires:  libadwaita-devel
BuildRequires:  enchant2-devel
BuildRequires:  libicu-devel
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib

Requires:       hicolor-icon-theme

%description
Text Editor is a simple text editor that focus on session management. It works
hard to keep track of changes and state even if you quit the application. You
can come back to your work even if you've never saved it to a file.


%prep
%autosetup


%build
%meson
%meson_build


%install
%meson_install

mkdir -p %{buildroot}%{_metainfodir}/
mv %{buildroot}%{_datadir}/appdata/%{uuid}.appdata.xml %{buildroot}%{_metainfodir}/%{uuid}.metainfo.xml
rmdir %{buildroot}%{_datadir}/appdata

%find_lang %{name}


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/%{uuid}.desktop


%files -f %{name}.lang
%license COPYING
%doc README.md
%{_bindir}/%{name}
%{_metainfodir}/%{uuid}.metainfo.xml
%{_datadir}/applications/%{uuid}.desktop
%{_datadir}/dbus-1/services/%{uuid}.service
%{_datadir}/glib-2.0/schemas/%{uuid}.gschema.xml
%{_datadir}/help/*/%{name}
%{_datadir}/icons/hicolor/*/*/*.svg


%changelog
%autochangelog
